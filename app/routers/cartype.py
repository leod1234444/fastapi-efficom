#system imports
import sys
sys.path.append("..")

#libraries imports
from fastapi import APIRouter
#local imports
from models.CarType import CarType

router = APIRouter()

# Get tous les car type
@router.get("/cartypes", tags=["cartypes"])
async def get_all_cartypes() -> list[CarType]:
    return cartypes

# Get un car type par son id
@router.get("/cartypes/{cartype_id}", tags=["cartypes"])
async def get_cartype_by_id(cartype_id: int):
    filtred_list = list(filter(lambda x: x["id"] == cartype_id, cartypes))
    return filtred_list


# Create un car type
@router.post("/cartypes", tags=["cartypes"])
async def create_cartype(cartype: CarType):
    cartypes.append(cartype)
    return cartype

# Update un car type
@router.put("/cartypes/{cartype_id}", tags=["cartypes"])
async def update_cartype(cartype_id: int, cartype: CarType):
    for i, c in enumerate(cartypes):
        if c["id"] == cartype_id:
            cartypes[i] = cartype
    return cartype

# Delete un car type
@router.delete("/cartypes/{cartype_id}", tags=["cartypes"])
async def delete_cartype(cartype_id: int):
    for i, c in enumerate(cartypes):
        if c["id"] == cartype_id:
            cartypes.pop(i)
    return {"message": "cartype deleted"}


cartypes = [
    CarType(id= 1, name= "Clio", model= "Clio", brand= "Renault", nbDoor= 5, engine= "1.5L"),
]
