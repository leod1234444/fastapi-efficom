#system imports
import sys
sys.path.append("..")

#libraries imports
from fastapi import APIRouter
#local imports
from models.User import User

router = APIRouter()

@router.get("/users", tags=["users"], response_model_exclude_unset=True)
async def get_all_users() -> list[User]:
    return users

@router.get("/users/search", tags=["users"])
async def search_users(name : str):
    return list(filter(lambda x: x["name"] == name, users))

@router.get("/users/{user_id}", tags=["users"])
async def get_user_by_id(user_id: int):
    for u in users:
        if u.id == user_id:
            return u
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@router.post("/users", tags=["users"])
async def create_user(user: User):
    users.append(user)
    return user

@router.put("/users/{user_id}", tags=["users"])
async def update_user(user_id: int, user: User):
    for i, u in enumerate(users):
        if u["id"] == user_id:
            users[i] = user
    return user

@router.delete("/users/{user_id}", tags=["users"])
async def delete_user(user_id: int):
    for i, u in enumerate(users):
        if u["id"] == user_id:
            users.pop(i)
    return {"message": "user deleted"}

users = [
    User(id= 1, name= "John", email= "de@de.de", password_hash= "9a1ef8e9abb02eb32f701f8a8b5e29ec83904b8767285bf788dbeef8be236e3c", tel= "123456789", newsletter= True, is_client= True),
    User(id= 2, name= "konan", email= "fr@fr.fr", password_hash= "bf0c97708b849de696e7373508b13c5ea92bafa972fc941d694443e494a4b84d", tel= "123456789", newsletter= True, is_client= True),
]