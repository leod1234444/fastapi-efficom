#system imports
import sys
sys.path.append("..")

#libraries imports
from fastapi import APIRouter, Response
#local imports
from models.Car import Car



router = APIRouter()

@router.get("/cars", tags=["cars"])
async def get_all_cars() -> list[Car]:
    if len(cars) == 0:
        Response(status_code=status.HTTP_204_NO_CONTENT)
    return cars

@router.get("/cars/{car_id}", tags=["cars"])
async def get_car_by_id(car_id: int):
    for u in cars:
        if u.id == car_id:
            return u
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@router.post("/cars", tags=["cars"], status_code=201)
async def create_car(car: Car):
    cars.append(car)
    return car

@router.put("/cars/{car_id}", tags=["cars"])
async def update_car(car_id: int, car: Car):
    for i, c in enumerate(cars):
        if c["id"] == car_id:
            cars[i] = car
    return car

@router.delete("/cars/{car_id}", tags=["cars"])
async def delete_car(car_id: int):
    for i, c in enumerate(cars):
        if c["id"] == car_id:
            cars.pop(i)
    return {"message": "car deleted"}

cars = [
    Car(id= 1,    
        km= 10000,
        color= "red",
        sellPrice= 15000,
        buyPrice= 10000,
        options= ["GPS", "climatisation"],
        state= "new",
        buildYear= "2020",
        parkingPlace= "Paris",
        comeDate= "2020-10-10",
        deliveryDate= "2020-10-10",
        saleEmployee= None,
        previousOwner= None,
        currentOwner= None
    ),
]