from fastapi.security import OAuth2PasswordRequestForm
from fastapi import Depends
from fastapi import APIRouter
from fastapi import HTTPException
import hashlib
import sys

from app.routers.user import users


router = APIRouter()

def hash_password(password: str):
    return hashlib.sha256(f'{password}'.encode('utf-8')).hexdigest()

@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    for u in users:
        if u.email == form_data.username:
            if u.password_hash == hash_password(form_data.password):
                return {"access_token": u.email, "token_type": "bearer"}

    raise HTTPException(status_code=400, detail="Incorrect email or password")