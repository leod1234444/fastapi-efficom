#system imports
from enum import Enum
#libraries imports
from pydantic import BaseModel
#local imports

class State(str, Enum):
    NEW = "new"
    USED = "used"
    RECONDITIONED = "reconditioned"