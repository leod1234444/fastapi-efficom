#system imports

#libraries imports
from pydantic import BaseModel
#local imports

class CarType(BaseModel):
    id: int
    name: str
    model: str
    brand: str
    nbDoor: int
    engine: str
