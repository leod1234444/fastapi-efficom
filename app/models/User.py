#system imports
#libraries imports
from pydantic import BaseModel
#local imports
class User(BaseModel):
    id: int
    name: str
    email: str
    password_hash: str
    tel: str 
    newsletter: bool
    is_client: bool