#system imports

#libraries imports plo
from pydantic import BaseModel
#local imports
from models.CarType import CarType
from models.Color import Color
from models.State import State
from models.User import User

class Car(BaseModel):
    id: int
    carType: CarType | None = None
    km: int
    color: Color
    sellPrice: float
    buyPrice: float | None = None
    options: list[str]
    state: State
    buildYear: str
    parkingPlace: str
    comeDate: str
    deliveryDate: str
    saleEmployee: User | None = None
    previousOwner: User | None = None
    currentOwner: User | None = None

def __init__(self, id, carType, km, color, sellPrice, buyPrice, options, state, buildYear, parkingPlace, comeDate, deliveryDate, saleEmployee, previousOwner, currentOwner):
    self.id = id
    self.carType = carType
    self.km = km
    self.color = color
    self.sellPrice = sellPrice
    self.buyPrice = buyPrice
    self.options = options
    self.state = state
    self.buildYear = buildYear
    self.parkingPlace = parkingPlace
    self.comeDate = comeDate
    self.deliveryDate = deliveryDate
    self.saleEmployee = saleEmployee
    self.previousOwner = previousOwner
    self.currentOwner = currentOwner