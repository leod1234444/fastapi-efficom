#system imports
from enum import Enum
#libraries imports
from pydantic import BaseModel
#local imports

class Color(str, Enum):
    RED = "red"
    BLUE = "blue"
    GREEN = "green"
    YELLOW = "yellow"
    WHITE = "white"
    BLACK = "black"