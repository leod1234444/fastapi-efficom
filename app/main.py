#system imports

#libraries imports
from fastapi import FastAPI
from pydantic import BaseModel
from enum import Enum
#local imports
from models.Car import Car
from routers.car import router as CarRouter
from routers.user import router as UserRouter
from routers.cartype import router as CarTypeRouter
from internal.auth import router as AuthRouter

app = FastAPI()

@app.get("/")
async def say_hello():
    """Ceci est une fonction qui dit bonjour oulala plz
    """
    return "bonjour"





# create a dataset for cartype
# cartypes = [
#     CarType(id= 1, name= "Clio", model= "Clio", brand= "Renault", nbDoor= 5, engine= "1.5L"),
# ]
# create a dataset for users
# users = [
#     User(id= 1, name= "John", email= "de@de.de", password_hash= "1234", tel= "123456789", newsletter= True, is_client= True),
# ]
# create a dataset for cars

app.include_router(CarRouter)
app.include_router(UserRouter)
app.include_router(CarTypeRouter)
app.include_router(AuthRouter)
